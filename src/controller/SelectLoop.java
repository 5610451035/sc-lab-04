package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import model.NestedLoop;
import view.GUI;

public class SelectLoop {
	
	public static void main(String[] args) {
	
		new SelectLoop();
	}

	
	public SelectLoop() {
		
		
		frame = new GUI();
		loop = new NestedLoop();
		frame.setListenerRun(listRun);
		frame.setListener(list);
		frame.setVisible(true);

	}
	
	GUI frame;
	ActionListener list = new ActionListener(){

		@Override
		public void actionPerformed(ActionEvent e) {
			
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			System.exit(0);
			
		}} ;
		
	NestedLoop loop;	
	ActionListener listRun = new ActionListener(){

		@Override
		public void actionPerformed(ActionEvent e) {
			int n=frame.getN();
			if (frame.getSelectCombo() == 0 ){
				String a = loop.form1(n) ;
				frame.setResult(a);
			} else if (frame.getSelectCombo() == 1) {
				String b = loop.form2(n) ;
				frame.setResult(b);
			} else if (frame.getSelectCombo() == 2) {
				String c = loop.form3(n) ;
				frame.setResult(c);
			} else {
				String d = loop.form4(n) ;
				frame.setResult(d);
			}
			
		}} ; 
}

