package model;

public class NestedLoop {
	
	String star="";
	public String form1(int n){
		
		for(int i=1; i<=(n-1); i++){
			for(int j=1 ; j<=n ; j++){ 
				star += "*";
			}
			star += "\n";
		}
		return star;
	}	
		
	public String form2(int n){
		for(int i=1; i<=n ;i++){
			for (int j=1;j<=(n-1);j++){
				star += "*";
			}
			star += "\n";
		}
		return star;
	}
		
	public String form3(int n){
		
		for(int i=1; i<=n ; i++){
			for (int j=1 ; j<=i ; j++){
				star += "*";
			}
			star += "\n";
		}
		return star;
	}
		
	public String form4(int n){
		
		for(int i=1; i<=(n-1) ; i++)
		{
			for (int j=1; j<=(n+2) ; j++)
			{
				if(j%2==0 ) {
					star += "*";
				}
				else {
					star += "-";
				}
			}
			star += "\n";
		}
		return star;
	}
		
		
		
		
		
		
	}

