package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Frame;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;


public class GUI extends JFrame {

	private JButton endButton = new JButton("Exit");;
	private JButton runButton = new JButton("RUN");
	private JTextField textfield = new JTextField("Enter number of star");
	private String str;
	private JComboBox box = new JComboBox();
	private JTextArea show = new JTextArea("____________RESULTS_____________");
	

	public GUI() {
		createframe();
	}

		
		
		public void createframe(){
			
			setTitle("-----WELCOME-----");
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			setSize(600,500);
		    setLayout(null);
			
			
			show.setBounds(30, 120, 410, 300);
			show.setBackground(Color.white);
			
			textfield.setBounds(30, 73, 410, 35);
			
			runButton.setBounds(470,28, 100, 300);
			endButton.setBounds(470,350, 100, 100);
		
			

			
			box.setBounds(30,30, 410, 30);
			box.addItem("Form1");
			box.addItem("Form2");
			box.addItem("Form3");
			box.addItem("Form4");
			
			
			
			add(runButton);
			add(endButton);
			add(show);
			add(box);
			add(textfield);
			
			setVisible(true);
			setResizable(false);
	}

	public void setResult(String str) {
		this.str = str;
		show.setText(str);
	}

	

	public void setListener(ActionListener list) {
		endButton.addActionListener(list);

	}
	public void setListenerRun(ActionListener listRun) {
		runButton.addActionListener(listRun);

	}



	public int getSelectCombo() {
		// TODO Auto-generated method stub
		return box.getSelectedIndex();
	}



	public int getN() {
		// TODO Auto-generated method stub
		return Integer.parseInt(textfield.getText());
	}

}